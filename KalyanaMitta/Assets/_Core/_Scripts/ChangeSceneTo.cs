﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

// Use this script to change asynchronously to any scene given as a string name.
public class ChangeSceneTo : MonoBehaviour {

    public string sceneToSwitchTo;
    public GameObject loadingCanvas;
    public Slider loadingSlider;

    public void ChangeTo()
    {
        ShowLoading();
        StartCoroutine(LoadSceneAsync());
    }

    IEnumerator LoadSceneAsync()
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneToSwitchTo);

        while (!asyncLoad.isDone)
        {
            loadingSlider.value = asyncLoad.progress;
            yield return null;
        }
    }

    public void disableButton()
    {
        gameObject.GetComponent<Button>().interactable = false;
    }

    public void ShowLoading()
    {
        if (loadingCanvas != null && loadingSlider != null)
        {
            loadingCanvas.SetActive(true);
            loadingSlider.gameObject.SetActive(true);
        }
    }
}
