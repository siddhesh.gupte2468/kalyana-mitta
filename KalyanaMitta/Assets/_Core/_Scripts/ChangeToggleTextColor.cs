﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeToggleTextColor : MonoBehaviour {

    private Toggle toggle;
    private Text text;

	// Use this for initialization
	void Start () {
        toggle = GetComponent<Toggle>();
        toggle.onValueChanged.AddListener(OnToggleValueChanged);

        text = GetComponent<Text>();
    }
	
    private void OnToggleValueChanged(bool isOn)
    {
        text.color = isOn ? new Color(1, 1, 1) : new Color(0.5f, 0.5f, 0.5f) ;
    }


	// Update is called once per frame
	void Update () {
		
	}
}
