﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class GazeToggle : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public Image progressImage; // add an image as child to your button object and set its image type to Filled. Assign it to this field in inspector.
    public bool isEntered = false;
    RectTransform rt;
    Button _button;
    Toggle toggleButton;
    float timeElapsed;
    Image cursor;
    // Use this for initialization
    void Awake()
    {
        toggleButton = GetComponent<Toggle>();
        //_button = GetComponent<Button>();
        rt = GetComponent<RectTransform>();

    }

    float GazeActivationTime = 3;

    void Update()
    {
        if (isEntered)
        {
            timeElapsed += Time.deltaTime;
            progressImage.fillAmount = Mathf.Clamp(timeElapsed / GazeActivationTime, 0, 1);
            if (timeElapsed >= GazeActivationTime)
            {
                timeElapsed = 0;
                //_button.onClick.Invoke();
                toggleButton.isOn = !toggleButton.isOn;
                progressImage.fillAmount = 0;
                isEntered = false;
            }
        }
        else
        {
            timeElapsed = 0;
        }
    }

    #region IPointerEnterHandler implementation

    public void OnPointerEnter(PointerEventData eventData)
    {
        isEntered = true;
    }

    #endregion

    #region IPointerExitHandler implementation

    public void OnPointerExit(PointerEventData eventData)
    {
        isEntered = false;
        progressImage.fillAmount = 0;
    }
    #endregion

}