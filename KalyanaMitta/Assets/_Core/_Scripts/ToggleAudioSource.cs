﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleAudioSource : MonoBehaviour {

    public GameObject soundObject;
    AudioSource audioSource;

    public void ChangeAudioStatus()
    {
        if (!audioSource)
        {
            audioSource = soundObject.GetComponent<AudioSource>();
        }

        audioSource.mute = !audioSource.mute;
    }
}
